USE [master]
GO

DROP DATABASE IF EXISTS [film]
GO
CREATE DATABASE [film]
GO

USE [film]
GO
CREATE SCHEMA [model] AUTHORIZATION [dbo]
GO


CREATE TABLE [model].[film_actor] 
(
[id_actor] INT NOT NULL,
[id_film] INT NOT NULL
CONSTRAINT PK_film_actor PRIMARY KEY (id_actor, id_film)
)
GO

CREATE TABLE [model].[film_category] 
(
[id_film] INT NOT NULL,
[id_category] INT NOT NULL
CONSTRAINT PK_film_category PRIMARY KEY (id_category, id_film)
)
GO

CREATE TABLE [model].[language] 
(
[id_language] INT IDENTITY PRIMARY KEY,
[name] VARCHAR(50) NOT NULL
)
GO

CREATE TABLE [model].[order]
(
 [id_order] INT IDENTITY PRIMARY KEY,
 [id_customer] INT NOT NULL,
 [id_employee] INT NOT NULL,
 [rental_date] DATE NOT NULL,
 [rental_due_date] DATE NOT NULL,
 [total_amount] DECIMAL NOT NULL,
 [order_type] VARCHAR(50) NOT NULL,
 [order_date] DATE NOT NULL
 )
 GO
  
CREATE TABLE [model].[order_film]
(
 [id_film] INT NOT NULL,
 [id_order] INT NOT NULL,
 [quantity] INT NOT NULL
 CONSTRAINT PK_order_film PRIMARY KEY (id_film, id_order)
 )
 GO 