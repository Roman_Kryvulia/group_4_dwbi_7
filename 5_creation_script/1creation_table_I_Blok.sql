
USE [film]
GO

CREATE TABLE [model].[placing]
(   
	id_placing              INT NOT NULL PRIMARY KEY,
	shelving                VARCHAR(50) NOT NULL,
	box                     VARCHAR(50) NOT NULL,
	quantity                INT NOT NULL 
)
GO


CREATE TABLE [model].[procurement]
(
	id_film                 INT NOT NULL,
	id_supplier             INT NOT NULL,
	date_of_procurement     DATETIME NOT NULL,
	quantity                INT NOT NULL 
	CONSTRAINT [PK_procurement] PRIMARY KEY ([id_film], [id_supplier], [date_of_procurement])
)
GO

CREATE TABLE [model].[studio]
(
	id_studio               INT NOT NULL PRIMARY KEY,
	name                    VARCHAR(50) NOT NULL,
	owner                   VARCHAR(50) NOT NULL,
	id_address              INT NOT NULL
)
GO

CREATE TABLE [model].[supplier]
(
	id_supplier             INT NOT NULL PRIMARY KEY,
	name                    VARCHAR(50) NOT NULL,
	surname                 VARCHAR(50) NOT NULL,
	company_name            VARCHAR(50) NOT NULL,
	id_address              INT NOT NULL,
	email                   VARCHAR(50) NOT NULL,
	phone_number            VARCHAR(50) NOT NULL
)
GO



