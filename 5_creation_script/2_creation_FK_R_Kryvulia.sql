USE [master]
GO
USE [film]
GO

ALTER TABLE [model].[film_actor]
ADD CONSTRAINT FK_film_actor_actor FOREIGN KEY(id_actor)
REFERENCES [model].[actor] (id_actor),
CONSTRAINT FK_film_actor_film FOREIGN KEY (id_film)
REFERENCES [model].[film](id_film)
GO

ALTER TABLE [model].[film_category]
ADD CONSTRAINT FK_film_category_film FOREIGN KEY (id_film)
REFERENCES [model].[film](id_film),
CONSTRAINT FK_film_category_category FOREIGN KEY(id_category)
REFERENCES [model].[category] (id_category)
GO

ALTER TABLE [model].[order]
ADD CONSTRAINT FK_order_customer FOREIGN KEY (id_customer)
REFERENCES [model].[customer](id_customer),
CONSTRAINT FK_order_employee FOREIGN KEY (id_employee)
REFERENCES [model].[employee](id_employee)
GO

ALTER TABLE [model].[order_film]
ADD CONSTRAINT FK_order_film_film FOREIGN KEY (id_film)
REFERENCES [model].[film](id_film),
CONSTRAINT FK_order_film_order FOREIGN KEY(id_order)
REFERENCES [model].[order] (id_order)
GO