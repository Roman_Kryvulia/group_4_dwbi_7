USE [film]
GO

ALTER TABLE [model].[film] add
       CONSTRAINT [FK_placing_film] FOREIGN KEY ([id_placing])
        REFERENCES [model].[placing]([id_placing])
GO

ALTER TABLE [model].[procurement] add
    CONSTRAINT [FK_procurement_supplier] FOREIGN KEY ([id_supplier])
        REFERENCES [model].[supplier]([id_supplier]),
    CONSTRAINT [FK_procurement_film] FOREIGN KEY ([id_film])
        REFERENCES [model].[film]([id_film])
GO

ALTER TABLE [model].[studio] add
       CONSTRAINT [FK_studio_address] FOREIGN KEY ([id_address])
        REFERENCES [model].[address]([id_address])
GO

ALTER TABLE [model].[supplier] add
        CONSTRAINT [FK_supplier_address] FOREIGN KEY ([id_address])
        REFERENCES [model].[address]([id_address])
GO