USE [Film]
GO


DROP TABLE IF EXISTS [Model].[actor];
GO


DROP TABLE IF EXISTS [Model].[address];
GO


DROP TABLE IF EXISTS [Model].[city];
GO


DROP TABLE IF EXISTS [Model].[country];
GO




DROP TABLE IF EXISTS [Model].[category];
GO

CREATE TABLE [Model].[actor]
(
 [id_actor]      INT NOT NULL ,
 [name]          VARCHAR(50) NOT NULL ,
 [surname]       VARCHAR(50) NOT NULL ,
 [gender]        VARCHAR(10) NOT NULL ,
 [date_of_birth] DATE NOT NULL ,
 [nationality]   VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_Actor] PRIMARY KEY  ([id_actor])
);
GO

CREATE TABLE [Model].[country]
(
 [id_country] INT NOT NULL ,
 [name]       VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_country] PRIMARY KEY  ([id_country])
);
GO




CREATE TABLE [Model].[category]
(
 [id_category] INT NOT NULL ,
 [name]        VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_Category] PRIMARY KEY  ([id_category])
);
GO




CREATE TABLE [Model].[city]
(
 [id_city]    INT NOT NULL ,
 [name]       VARCHAR(50) NOT NULL ,
 [id_country] INT NOT NULL ,

 CONSTRAINT [PK_city] PRIMARY KEY  ([id_city]),
 CONSTRAINT [FK_country] FOREIGN KEY ([id_country])
  REFERENCES [Model].[country]([id_country])
);
GO


CREATE TABLE [Model].[address]
(
 [id_address]   INT NOT NULL ,
 [adress_line1] VARCHAR(50) NOT NULL ,
 [adress_line2] VARCHAR(50) NULL ,
 [postal_code]  VARCHAR(50) NOT NULL ,
 [region]       VARCHAR(50) NOT NULL ,
 [id_city]      INT NOT NULL ,

 CONSTRAINT [PK_Address] PRIMARY KEY  ([id_address]),
 CONSTRAINT [FK_city] FOREIGN KEY ([id_city])
  REFERENCES [Model].[city]([id_city])
);
GO





