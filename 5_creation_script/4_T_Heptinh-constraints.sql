use film
go

drop table if exists model.customer
go

create table model.customer
	(id_customer int identity(1,1) NOT NULL,
	 name varchar(50) NOT NULL, 
	 surname varchar(50) NOT NULL,
	 gender char(1) NOT NULL, --it can be either 'f' for female and 'm' for male
	 id_address int NOT NULL ,
	 phone_number char(10) NOT NULL,
	 date_of_registration date NOT NULL DEFAULT getdate(),
	 email varchar(50) NOT NULL,
	 birth_date date NOT NULL
	)
go

drop table if exists model.discount_card
go

create table model.discount_card
	(id_card int identity(1,1) NOT NULL,
	 id_customer int NOT NULL,
	 accumulation decimal(4,2) NOT NULL
	)
go

drop table if exists model.employee
go

create table model.employee
	(id_employee int identity(1,1) NOT NULL,
	 name varchar(50) NOT NULL,
	 surname varchar(50) NOT NULL,
	 gender char(1) NOT NULL, --it can be either 'f' for female and 'm' for male
	 birth_date date NOT NULL,
	 email varchar(50) NOT NULL,
	 [user_name] varchar(50) NOT NULL,
	 salary decimal(7, 2) NOT NULL,
	 position varchar(20) NOT NULL,
	 hire_date date NOT NULL,
	 id_address int NOT NULL
	)
go

drop table if exists model.film
go

create table model.film
	(id_film int identity(1,1) NOT NULL,
	 title varchar(50) NOT NULL,
	 release_year date NOT NULL,
	 [length] int NOT NULL,
	 [description] varchar(200) NULL,
	 budget int NOT NULL,
	 id_language int NOT NULL,
	 award varchar(50) NULL,
	 id_studio int NOT NULL,
	 price decimal(5,2) NOT NULL,
	 imdb_rating decimal(2,1) NOT NULL,
	 id_placing int NOT NULL
	)
go


alter table model.customer
add primary key (id_customer)
go

alter table model.discount_card
add primary key (id_card)
go

alter table model.employee
add primary key (id_employee)
go

alter table model.film
add primary key (id_film)
go


