
use film
go

alter table model.customer
add check (gender in ('f', 'm'))
go

alter table model.customer
add foreign key (id_address) references model.[address](id_address)
go

alter table model.customer
add unique(phone_number)
go

--constraints for model.discount_card table


alter table model.discount_card
add foreign key (id_customer) references model.customer(id_customer)
go

--constraints for model.employee table


alter table model.employee
add check (gender in ('f', 'm'))
go

alter table model.employee
add constraint
UK_employee unique(email, [user_name])
go


alter table model.employee
add foreign key (id_address) references model.[address](id_address)
go


--constraints for model.film table


alter table model.film
add foreign key (id_language) references model.[language](id_language)
go

alter table model.film
add foreign key (id_studio) references model.[studio](id_studio)
go
