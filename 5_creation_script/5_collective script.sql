--creation tables R_Kryvulia
USE [master]
GO

DROP DATABASE IF EXISTS [film]
GO
CREATE DATABASE [film]
GO

USE [film]
GO
CREATE SCHEMA [model] AUTHORIZATION [dbo]
GO


CREATE TABLE [model].[film_actor] 
(
[id_actor] INT NOT NULL,
[id_film] INT NOT NULL
CONSTRAINT PK_film_actor PRIMARY KEY (id_actor, id_film)
)
GO

CREATE TABLE [model].[film_category] 
(
[id_film] INT NOT NULL,
[id_category] INT NOT NULL
CONSTRAINT PK_film_category PRIMARY KEY (id_category, id_film)
)
GO

CREATE TABLE [model].[language] 
(
[id_language] INT IDENTITY PRIMARY KEY,
[name] VARCHAR(50) NOT NULL
)
GO

CREATE TABLE [model].[order]
(
 [id_order] INT IDENTITY PRIMARY KEY,
 [id_customer] INT NOT NULL,
 [id_employee] INT NOT NULL,
 [rental_date] DATE NOT NULL,
 [rental_due_date] DATE NOT NULL,
 [total_amount] DECIMAL NOT NULL,
 [order_type] VARCHAR(50) NOT NULL,
 [order_date] DATE NOT NULL
 )
 GO
  
CREATE TABLE [model].[order_film]
(
 [id_film] INT NOT NULL,
 [id_order] INT NOT NULL,
 [quantity] INT NOT NULL
 CONSTRAINT PK_order_film PRIMARY KEY (id_film, id_order)
 )
 GO 
  
  
  --creation tables I_Block

  
USE [film]
GO

CREATE TABLE [model].[placing]
(   
	id_placing              INT NOT NULL PRIMARY KEY,
	shelving                VARCHAR(50) NOT NULL,
	box                     VARCHAR(50) NOT NULL,
	quantity                INT NOT NULL 
)
GO


CREATE TABLE [model].[procurement]
(
	id_film                 INT NOT NULL,
	id_supplier             INT NOT NULL,
	date_of_procurement     DATETIME NOT NULL,
	quantity                INT NOT NULL 
	CONSTRAINT [PK_procurement] PRIMARY KEY ([id_film], [id_supplier], [date_of_procurement])
)
GO

CREATE TABLE [model].[studio]
(
	id_studio               INT NOT NULL PRIMARY KEY,
	name                    VARCHAR(50) NOT NULL,
	owner                   VARCHAR(50) NOT NULL,
	id_address              INT NOT NULL
)
GO

CREATE TABLE [model].[supplier]
(
	id_supplier             INT NOT NULL PRIMARY KEY,
	name                    VARCHAR(50) NOT NULL,
	surname                 VARCHAR(50) NOT NULL,
	company_name            VARCHAR(50) NOT NULL,
	id_address              INT NOT NULL,
	email                   VARCHAR(50) NOT NULL,
	phone_number            VARCHAR(50) NOT NULL
)
GO

 --creation table T_Heptinh
 use film
go

drop table if exists model.customer
go

create table model.customer
	(id_customer int identity(1,1) NOT NULL,
	 name varchar(50) NOT NULL, 
	 surname varchar(50) NOT NULL,
	 gender char(1) NOT NULL, --it can be either 'f' for female and 'm' for male
	 id_address int NOT NULL ,
	 phone_number char(10) NOT NULL,
	 date_of_registration date NOT NULL DEFAULT getdate(),
	 email varchar(50) NOT NULL,
	 birth_date date NOT NULL
	)
go

drop table if exists model.discount_card
go

create table model.discount_card
	(id_card int identity(1,1) NOT NULL,
	 id_customer int NOT NULL,
	 accumulation decimal(4,2) NOT NULL
	)
go

drop table if exists model.employee
go

create table model.employee
	(id_employee int identity(1,1) NOT NULL,
	 name varchar(50) NOT NULL,
	 surname varchar(50) NOT NULL,
	 gender char(1) NOT NULL, --it can be either 'f' for female and 'm' for male
	 birth_date date NOT NULL,
	 email varchar(50) NOT NULL,
	 [user_name] varchar(50) NOT NULL,
	 salary decimal(7, 2) NOT NULL,
	 position varchar(20) NOT NULL,
	 hire_date date NOT NULL,
	 id_address int NOT NULL
	)
go

drop table if exists model.film
go

create table model.film
	(id_film int identity(1,1) NOT NULL,
	 title varchar(50) NOT NULL,
	 release_year date NOT NULL,
	 [length] int NOT NULL,
	 [description] varchar(200) NULL,
	 budget int NOT NULL,
	 id_language int NOT NULL,
	 award varchar(50) NULL,
	 id_studio int NOT NULL,
	 price decimal(5,2) NOT NULL,
	 imdb_rating decimal(2,1) NOT NULL,
	 id_placing int NOT NULL
	)
go


alter table model.customer
add primary key (id_customer)
go

alter table model.discount_card
add primary key (id_card)
go

alter table model.employee
add primary key (id_employee)
go

alter table model.film
add primary key (id_film)
go
 



--creation tables+FK D_Obarianyk
USE [Film]
GO


DROP TABLE IF EXISTS [Model].[actor];
GO


DROP TABLE IF EXISTS [Model].[address];
GO


DROP TABLE IF EXISTS [Model].[city];
GO


DROP TABLE IF EXISTS [Model].[country];
GO




DROP TABLE IF EXISTS [Model].[category];
GO

CREATE TABLE [Model].[actor]
(
 [id_actor]      INT NOT NULL ,
 [name]          VARCHAR(50) NOT NULL ,
 [surname]       VARCHAR(50) NOT NULL ,
 [gender]        VARCHAR(10) NOT NULL ,
 [date_of_birth] DATE NOT NULL ,
 [nationality]   VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_Actor] PRIMARY KEY  ([id_actor])
);
GO

CREATE TABLE [Model].[country]
(
 [id_country] INT NOT NULL ,
 [name]       VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_country] PRIMARY KEY  ([id_country])
);
GO




CREATE TABLE [Model].[category]
(
 [id_category] INT NOT NULL ,
 [name]        VARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_Category] PRIMARY KEY  ([id_category])
);
GO




CREATE TABLE [Model].[city]
(
 [id_city]    INT NOT NULL ,
 [name]       VARCHAR(50) NOT NULL ,
 [id_country] INT NOT NULL ,

 CONSTRAINT [PK_city] PRIMARY KEY  ([id_city]),
 CONSTRAINT [FK_country] FOREIGN KEY ([id_country])
  REFERENCES [Model].[country]([id_country])
);
GO


CREATE TABLE [Model].[address]
(
 [id_address]   INT NOT NULL ,
 [adress_line1] VARCHAR(50) NOT NULL ,
 [adress_line2] VARCHAR(50) NULL ,
 [postal_code]  VARCHAR(50) NOT NULL ,
 [region]       VARCHAR(50) NOT NULL ,
 [id_city]      INT NOT NULL ,

 CONSTRAINT [PK_Address] PRIMARY KEY  ([id_address]),
 CONSTRAINT [FK_city] FOREIGN KEY ([id_city])
  REFERENCES [Model].[city]([id_city])
);
GO

--creation constraint I_Block 
USE [film]
GO

ALTER TABLE [model].[film] add
       CONSTRAINT [FK_placing_film] FOREIGN KEY ([id_placing])
        REFERENCES [model].[placing]([id_placing])
GO

ALTER TABLE [model].[procurement] add
    CONSTRAINT [FK_procurement_supplier] FOREIGN KEY ([id_supplier])
        REFERENCES [model].[supplier]([id_supplier]),
    CONSTRAINT [FK_procurement_film] FOREIGN KEY ([id_film])
        REFERENCES [model].[film]([id_film])
GO

ALTER TABLE [model].[studio] add
       CONSTRAINT [FK_studio_address] FOREIGN KEY ([id_address])
        REFERENCES [model].[address]([id_address])
GO

ALTER TABLE [model].[supplier] add
        CONSTRAINT [FK_supplier_address] FOREIGN KEY ([id_address])
        REFERENCES [model].[address]([id_address])
GO

--creation constraint T_Heptinh
use film
go

alter table model.customer
add check (gender in ('f', 'm'))
go

alter table model.customer
add foreign key (id_address) references model.[address](id_address)
go

alter table model.customer
add unique(phone_number)
go

--constraints for model.discount_card table


alter table model.discount_card
add foreign key (id_customer) references model.customer(id_customer)
go

--constraints for model.employee table


alter table model.employee
add check (gender in ('f', 'm'))
go

alter table model.employee
add constraint
UK_employee unique(email, [user_name])
go


alter table model.employee
add foreign key (id_address) references model.[address](id_address)
go


--constraints for model.film table


alter table model.film
add foreign key (id_language) references model.[language](id_language)
go

alter table model.film
add foreign key (id_studio) references model.[studio](id_studio)
go


--creation constraint R_Kryvulia
USE [master]
GO
USE [film]
GO

ALTER TABLE [model].[film_actor]
ADD CONSTRAINT FK_film_actor_actor FOREIGN KEY(id_actor)
REFERENCES [model].[actor] (id_actor),
CONSTRAINT FK_film_actor_film FOREIGN KEY (id_film)
REFERENCES [model].[film](id_film)
GO

ALTER TABLE [model].[film_category]
ADD CONSTRAINT FK_film_category_film FOREIGN KEY (id_film)
REFERENCES [model].[film](id_film),
CONSTRAINT FK_film_category_category FOREIGN KEY(id_category)
REFERENCES [model].[category] (id_category)
GO

ALTER TABLE [model].[order]
ADD CONSTRAINT FK_order_customer FOREIGN KEY (id_customer)
REFERENCES [model].[customer](id_customer),
CONSTRAINT FK_order_employee FOREIGN KEY (id_employee)
REFERENCES [model].[employee](id_employee)
GO

ALTER TABLE [model].[order_film]
ADD CONSTRAINT FK_order_film_film FOREIGN KEY (id_film)
REFERENCES [model].[film](id_film),
CONSTRAINT FK_order_film_order FOREIGN KEY(id_order)
REFERENCES [model].[order] (id_order)
GO






